#!/usr/bin/env python

import os
import sys

def is_good(i):
    s = str(i)
    mc = 0
    seen_d = False
    prev = -1
    for d in s:
        int_d = int(d)
        if int_d == prev:
            mc += 1
        elif prev is not None:
            if int_d < prev:
                return False
            if mc == 2:
                seen_d = True
            mc = 1
        prev = int_d
    return seen_d or mc == 2


def main(lines):
    l = lines[0]
    a, b = [int(i) for i in l.split('-')]
    gc = 0
    for i in range(a, b + 1):
        if is_good(i):
            gc += 1
    print(gc)



if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

