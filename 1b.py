#!/usr/bin/env python

def weight(w):
    return max(0, (w // 3) - 2)

def total_weight(w):
    fuel_weight = weight(w)
    s = 0
    while fuel_weight:
        s += fuel_weight
        fuel_weight = weight(fuel_weight)
    return s


def main(lines):
    s = 0
    for l in lines:
        i = int(l)
        s += total_weight(i)
    print(s)


if __name__ == '__main__':
    with open('1.in') as f:
        lines = [l.strip() for l in f]
        main(lines)
