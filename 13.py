#!/usr/bin/env python

import os
import sys
import json
import intcode
import time



def main(lines):
    p = intcode.Program(lines[0], [])
    p.data[0] = 2
    score = 0

    def pscreen():
        print(score)
        for l in screen:
            print(''.join(l))

    def get_t(t):
        return [' ', 'W', 'B', '_', '.'][t]
    screen = []
    for y in range(20):
        screen.append([''] * 44)
    frame = 0
    paddle_x = 0
    ball_x = 0
    while not p.finished:
        o = p.run()
        chunks = [o[i:i+3] for i in range(0, len(o), 3)]
        for c in chunks:
            x, y, t = c
            if x == -1 and y == 0:
                score = t
            else:
                tile = get_t(t)
                if tile == '_':
                    paddle_x = x
                elif tile == '.':
                    ball_x = x

                screen[y][x] = get_t(t)
        pscreen()

        if paddle_x < ball_x:
            i = 1
        elif paddle_x > ball_x:
            i = -1
        else:
            i = 0
        p.add_input([i])
        frame += 1
        input()


if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

