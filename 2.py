#!/usr/bin/env python

import operator

def run_program(data):
    data = list(data)
    pc = 0
    while True:
        opcode = data[pc]
        if opcode == 99:
            break
        if opcode in [1, 2]:
            fn = [operator.add, operator.mul][opcode - 1]
            r = fn(data[data[pc + 1]], data[data[pc + 2]])
            data[data[pc + 3]] = r
        pc += 4
    return data

def main(data):
    for x in range(100):
        for y in range(100):
            data[1] = x
            data[2] = y
            result = run_program(data)[0]
            if result == 19690720:
                print(100 * x + y)




if __name__ == '__main__':
    with open('2.in') as f:
        lines = [l.strip() for l in f]
    main([int(i) for i in lines[0].split(',')])
