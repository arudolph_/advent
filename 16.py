#!/usr/bin/env python

import os
import sys
import intcode

def phase2(lines):
    digits = [int(d) for d in lines[0]]
    output_ix = int(''.join(str(d) for d in digits[:7])) - 1
    digits = digits * 10000
    digits = digits[output_ix:]


    def phase(digits):
        new_ds = [0] * len(digits)
        s = sum(digits) % 10
        new_ds[0] = s
        for ix in range(1, len(digits)):
            s -= digits[ix - 1]
            s %= 10
            new_ds[ix] = s
        return new_ds

    for i in range(100):
        digits = phase(digits)
    print(''.join(str(d) for d in digits[1:9]))






def main(lines):
    phase2(lines)

def phase1(lines):
    digits = [int(d) for d in lines[0]]

    pattern = [0, 1, 0, -1]
    def compute_digit(ix, digits):
        pat = pattern
        if ix >= 1:
            pat = [sd for d in pattern for sd in [d] * (ix + 1)]
        d = sum(digits[ix] * pat[(ix + 1) % len(pat)] for
                ix in range(len(digits)))
        return abs(d) % 10

    def phase(digits):
        return [compute_digit(ix, digits) for ix in range(len(digits))]

    for i in range(100):
        print(i)
        digits = phase(digits)


    print(''.join(str(d) for d in digits[:8]))




if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

