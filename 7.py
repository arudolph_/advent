#!/usr/bin/env python

import os
import sys
import operator
import itertools


class Program:
    def __init__(self, data, input):
        self.data = list(data)
        self.input = input
        self.pc = 0
        self.ic = 0
        self.finished = False

    def add_input(self, input):
        self.input += input

    def run(self):
        assert not self.finished
        data = self.data
        pc = self.pc
        ic = self.ic
        max_params = 3

        def get_param(pos):
            if modes[pos - 1]:
                return data[pc + pos]
            else:
                return data[data[pc + pos]]

        output = []

        while True:
            inst = data[pc]
            opcode = inst % 100
            param_modes = inst // 100
            modes = []
            for i in range(max_params):
                modes.append(bool(param_modes & 1))
                param_modes //= 10

            if opcode == 99:
                self.finished = True
                break
            elif opcode in [1, 2]:
                fn = [operator.add, operator.mul][opcode - 1]
                r = fn(get_param(1), get_param(2))
                data[data[pc + 3]] = r
                pc += 4
            elif opcode == 3:
                if ic >= len(self.input):
                    break
                data[data[pc + 1]] = self.input[ic]
                ic += 1
                pc += 2
            elif opcode == 4:
                output.append(get_param(1))
                pc += 2
            elif opcode in [5, 6]:
                tv = get_param(1)
                tvz = tv == 0
                jiz = opcode == 6
                if tvz == jiz:
                    pc = get_param(2)
                else:
                    pc += 3
            elif opcode in [7, 8]:
                fn = [operator.lt, operator.eq][opcode - 7]
                data[data[pc + 3]] = int(fn(get_param(1), get_param(2)))
                pc += 4

        self.pc = pc
        self.ic = ic

        return output

def find_amplifier_settings(data):
    max_out = None
    for p in itertools.permutations(range(5, 10)):
        programs = [None] * 5
        input = 0
        done = False
        while not done:
            for ix in range(5):
                if not programs[ix]:
                    programs[ix] = Program(data, [p[ix]])
                programs[ix].add_input([input])
                output = programs[ix].run()
                assert len(output) == 1
                input = output[0]

            if programs[4].finished:
                done = True

        if max_out is None or max_out < input:
            max_out = input
    print(max_out)


def main(lines):
    data = [int(i) for i in lines[0].split(',')]
    find_amplifier_settings(data)

if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

