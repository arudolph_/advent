#!/usr/bin/env python

import os
import sys
import math
import itertools


def main(lines):
    def make_row(r):
        return [c == '#' for c in r]
    grid = [make_row(r) for r in lines]
    xd = len(grid[0])
    yd = len(grid)

    asteroids = set()
    for y in range(yd):
        for x in range(xd):
            if grid[y][x]:
                asteroids.add((x, y))

    a, r = part1(asteroids)
    print(r)
    print(part2(asteroids, a))

def part2(asteroids, base):
    asteroids = set(asteroids)
    asteroids.remove(base)

    def angle(a):
        return math.atan2(a[0] - base[0], base[1] - a[1]) % (math.pi * 2)


    def d(a):
        return math.hypot(*[ac - bc for ac, bc in zip(a, base)])

    buckets = {k: list(sorted(v, key=d)) for k, v in
               itertools.groupby(sorted(asteroids, key=angle), key=angle)}

    def cw():
        tc = len(asteroids)
        c = 0
        r = 0
        while c < tc:
            for b in sorted(buckets):
                p = buckets[b]
                if r < len(p):
                    c += 1
                    yield p[r]
            r += 1

    return list(x for x in cw())[199]

def foo():

    ds = list(ds)
    def key(p):
        x, y = p
        if x >= 0 and y < 0:
            return (0, 0, 0, x / -y + 1)
        elif x > 0 and y >= 0:
            return (0, 0, y / x + 1, 0)
        elif x <= 0 and y > 0:
            return (0, -x / y + 1, 0, 0)
        else:
            return (-y / -x + 1, 0, 0, 0)
    ds.sort(key=key)
    hit_c = 0
    hit = set()
    while len(hit) < len(asteroids) - 1:
        for d in ds:
            dx, dy = d
            x, y = base
            x += dx
            y += dy
            hit_dir = False
            while not hit_dir and 0 <= x < xd and 0 <= y < yd:
                if (x, y) in asteroids and (x, y) not in hit:
                    hit_c += 1
                    if hit_c == 200:
                        return (x, y)
                    hit.add((x, y))
                    hit_dir = True
                x += dx
                y += dy





def get_ds(a, b):
    dx, dy = b[0] - a[0], b[1] - a[1]
    gcd = math.gcd(dx, dy)
    return (dx // gcd, dy // gcd)

def part1(asteroids):
    def visible(a):
        return len(set(get_ds(a, b) for b in asteroids if b != a))
    a = max(asteroids, key=visible)
    return (a, visible(a))





if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

