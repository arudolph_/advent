#!/usr/bin/env python

import os
import sys
import intcode

def paint(prog):
    loc = (0, 0)
    grid = {loc: 1}
    painted = set()

    dirs = [(0, -1), (1, 0), (0, 1), (-1, 0)]
    dx = 0

    def turn(dir):
        nonlocal dx, loc
    while not prog.finished:
        c, dir = prog.run_with_input([grid.get(loc, 0)])
        grid[loc] = c
        d = -1 if dir == 0 else 1
        dx = (dx + d) % len(dirs)
        loc = (loc[0] + dirs[dx][0], loc[1] + dirs[dx][1])

    print(len(grid))
    xs = [p[0] for p in grid]
    ys = [p[1] for p in grid]

    def get_coord(x, y):
        return 'X' if grid.get((x, y), 0) == 1 else ' '

    for y in range(min(ys), max(ys) + 1):
        print(''.join(get_coord(x, y) for x in range(min(xs), max(xs) + 1)))


def main(lines):
    p = intcode.Program(lines[0])
    paint(p)

if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

