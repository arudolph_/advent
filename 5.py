#!/usr/bin/env python

import os
import sys
import operator

def run_program(data, input):
    data = list(data)
    pc = 0
    ic = 0
    max_params = 3

    def get_param(pos):
        if modes[pos - 1]:
            return data[pc + pos]
        else:
            return data[data[pc + pos]]

    while True:
        inst = data[pc]
        opcode = inst % 100
        param_modes = inst // 100
        modes = []
        for i in range(max_params):
            modes.append(bool(param_modes & 1))
            param_modes //= 10

        if opcode == 99:
            break
        elif opcode in [1, 2]:
            fn = [operator.add, operator.mul][opcode - 1]
            r = fn(get_param(1), get_param(2))
            data[data[pc + 3]] = r
            pc += 4
        elif opcode == 3:
            data[data[pc + 1]] = input[ic]
            ic += 1
            pc += 2
        elif opcode == 4:
            print(get_param(1))
            pc += 2
        elif opcode in [5, 6]:
            tv = get_param(1)
            tvz = tv == 0
            jiz = opcode == 6
            if tvz == jiz:
                pc = get_param(2)
            else:
                pc += 3
        elif opcode in [7, 8]:
            fn = [operator.lt, operator.eq][opcode - 7]
            data[data[pc + 3]] = int(fn(get_param(1), get_param(2)))
            pc += 4

    return data



def main(lines):
    data = [int(i) for i in lines[0].split(',')]
    data = run_program(data, [5])

if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

