#!/usr/bin/env python

def main(lines):
    s = 0
    for l in lines:
        i = int(l)
        s += (i // 3) - 2
    print(s)


if __name__ == '__main__':
    with open('1.in') as f:
        lines = [l.strip() for l in f]
        main(lines)
