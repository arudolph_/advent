#!/usr/bin/env python

import os
import sys
import re
import itertools


class Moon:
    def __init__(self, x, y ,z):
        self.pos = (x, y, z)
        self.v = [0, 0, 0]

    def apply(self):
        p = self.pos
        v = self.v
        self.pos = (p[0] + v[0], p[1] + v[1], p[2] + v[2])

    def pot(self):
        return sum(abs(v) for v in self.pos)

    def kin(self):
        return sum(abs(v) for v in self.v)

    def energy(self):
        return self.pot() * self.kin()


def addv(m1, m2):
    for i in range(3):
        if m1.pos[i] < m2.pos[i]:
            m1.v[i] += 1
            m2.v[i] -= 1
        elif m1.pos[i] > m2.pos[i]:
            m2.v[i] += 1
            m1.v[i] -= 1

def main(lines):
    moons = []
    for l in lines:
        parts = l.split(',')
        def fix(p):
            return int(re.sub('[^0-9-]', '', p))
        parts = [fix(p) for p in parts]
        moons.append(Moon(*parts))
    step = 0
    def axis_ps(i):
        return ':'.join(str(m.pos[i]) for m in moons)
    istates = [axis_ps(i) for i in range(3)]

    periods = {}
    while len(periods) < 3:
        for m1, m2 in itertools.combinations(moons, 2):
            addv(m1, m2)
        for m in moons:
            m.apply()
        step += 1
        for i in range(3):
            ps = axis_ps(i)
            if istates[i] == ps and m.v[i] == 0:
                if i not in periods:
                    periods[i] = step
                print(i, step)

    import math
    gcd = math.gcd
    def lcm(a, b):
        return a // gcd(a, b) * b
    a, b, c = periods.values()

    print(lcm(a, lcm(b, c)))


    #print(':'.join(str(m) for m in moons))
    #print(sum(m.energy() for m in moons))






if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

