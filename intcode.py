#!/usr/bin/env python

import sys
from enum import IntEnum
import operator
import collections


class AddrMode(IntEnum):
    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2


class Program:
    def __init__(self, line, input=[]):
        self.data = {ix: int(i) for ix, i in enumerate(line.split(','))}
        self.input = collections.deque(input)
        self.output = []
        self.pc = 0
        self.rb = 0
        self.finished = False

    def add_input(self, input):
        self.input += input

    def run_with_input(self, input):
        self.add_input(input)
        return self.run()

    def run(self):
        assert not self.finished
        data = self.data
        pc = self.pc
        max_params = 3

        def get_param_addr(pos):
            mode = modes[pos - 1]
            if mode == AddrMode.IMMEDIATE:
                return pc + pos
            elif mode == AddrMode.RELATIVE:
                return data.get(pc + pos, 0) + self.rb
            else:
                assert mode == AddrMode.POSITION
                return data.get(pc + pos, 0)

        def write_param(pos, v):
            assert modes[pos - 1] != AddrMode.IMMEDIATE
            data[get_param_addr(pos)] = v

        def get_param(pos):
            return data.get(get_param_addr(pos), 0)

        self.output = []

        while True:
            inst = data[pc]
            opcode = inst % 100
            param_modes = inst // 100
            modes = []
            for i in range(max_params):
                modes.append(AddrMode(param_modes % 10))
                param_modes //= 10

            if opcode == 99:
                self.finished = True
                break
            elif opcode in [1, 2]:
                fn = [operator.add, operator.mul][opcode - 1]
                r = fn(get_param(1), get_param(2))
                write_param(3, r)
                pc += 4
            elif opcode == 3:
                if not self.input:
                    break
                write_param(1, self.input.popleft())
                pc += 2
            elif opcode == 4:
                self.output.append(get_param(1))
                pc += 2
            elif opcode in [5, 6]:
                tv = get_param(1)
                tvz = tv == 0
                jiz = opcode == 6
                if tvz == jiz:
                    pc = get_param(2)
                else:
                    pc += 3
            elif opcode in [7, 8]:
                fn = [operator.lt, operator.eq][opcode - 7]
                write_param(3, int(fn(get_param(1), get_param(2))))
                pc += 4
            elif opcode == 9:
                self.rb += get_param(1)
                pc += 2

        self.pc = pc

        return self.output

def main(lines):
    with open(sys.argv[1]) as f:
        lines = [l.strip() for l in f]
        data = [int(i) for i in lines[0].split(',')]
    d_input = map(int, sys.argv[2:])
    prog = Program(data, d_input)
    print(prog.run())

if __name__ == '__main__':
    main()

