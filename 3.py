#!/usr/bin/env python

def visit(l, cb):
    coord = (0, 0)
    deltas = {'R': (1, 0), 'L': (-1, 0), 'U': (0, 1), 'D': (0, -1)}

    ds = l.split(',')
    ts = 0
    for d in ds:
        dir = d[0]
        l = int(d[1:])
        delta = deltas[dir]
        for _ in range(l):
            coord = (coord[0] + delta[0], coord[1] + delta[1])
            ts += 1
            cb(coord, ts)

def main(lines):
    s = {}

    def first_cb(coord, ts):
        s[coord] = ts

    visit(lines[0], first_cb)

    min_dist = None
    def second_cb(coord, ts):
        nonlocal min_dist
        if coord in s:
            dist = ts + s[coord]
            if min_dist is None or dist < min_dist:
                min_dist = dist

    visit(lines[1], second_cb)
    print(min_dist)




if __name__ == '__main__':
    with open('3.in') as f:
        lines = [l.strip() for l in f]
    main(lines)
