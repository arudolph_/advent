#!/usr/bin/env python

import os
import sys
import intcode
import itertools
import functools
import math


def main(lines):
    pass


if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.rstrip('\n') for l in f]
    main(lines)

