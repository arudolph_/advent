#!/usr/bin/env python

import os
import sys
import intcode
import itertools


def main(lines):
    part2(lines)

def part2(lines):
    p = intcode.Program(lines[0], [])

    def ascii_line(l):
        return [ord(c) for c in l]

    lines = [
        'A,A,B,C,B,C,B,C,B,A',
        'R,6,L,12,R,6',
        'L,12,R,6,L,8,L,12',
        'R,12,L,10,L,10',
        'n',
        '',
    ]

    in_data = ascii_line('\n'.join(lines))

    p.add_input(in_data)

    o = p.run()
    print(o)
    #print(''.join(chr(c) for c in o))



def part1(lines):
    p = intcode.Program(lines[0], [])
    o = p.run()
    s = ''.join(chr(c) for c in o)

    g = [[c for c in l] for l in s.split('\n')]

    gm = {}
    for iy in range(len(g)):
        for ix, c in enumerate(g[iy]):
            gm[(ix, iy)] = c
    gm = {k: gm[k] for k in gm if gm[k] != '.'}

    ds = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    def find_intersections():
        ds = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        def is_good(x, y):
            if gm[(x, y)] == '#':
                for dx, dy in ds:
                    nx, ny = x + dx, y + dy
                    if gm.get((nx, ny)) != '#':
                        return False
                return True
            return False

        return set(p for p in gm if is_good(*p))
    intersections = find_intersections()

    x, y = [k for k in gm if gm[k] in '^v<>'][0]
    ds = {'^': (0, -1), 'v': (0, 1), '<': (-1, 0), '>' :(1, 0)}
    reverse_ds = {v: k for k, v in ds.items()}
    right_map = {(-1, 0): (0, -1), (0, -1): (1, 0), (1, 0): (0, 1),
                 (0, 1): (-1, 0)}
    left_map = {v: k for k, v in right_map.items()}

    startdir = gm[(x, y)]
    cur_d = ds[gm[(x, y)]]
    def find_valid_dirs(x, y):
        ret = []
        def is_good(d):
            dx, dy = d
            return gm.get((x + dx, y + dy)) == '#'
        return [d for d in ds.values() if is_good(d)]
    def find_run_from(dir, x, y):
        vds = find_valid_dirs(x, y)
        def handle_run(d):
            l = 0
            sx, sy = x, y
            while True:
                sx += d[0]
                sy += d[1]
                if gm.get((sx, sy)) != '#':
                    break
                l += 1
            return (l, sx - d[0], sy - d[1])

        for d in vds:
            if right_map.get(dir) == d:
                return ('R', d, handle_run(d))
            if left_map.get(dir) == d:
                return ('L', d, handle_run(d))

    print(find_run_from((1, 0), 6, 16))
    runs = []
    while True:
        run = find_run_from(cur_d, x, y)
        if not run:
            break
        t, cur_d, (l, x, y) = run
        runs.append((t, l))
    for r in runs:
        print(f'{r[0]},{r[1]}')








if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

