#!/usr/bin/env python

import os
import sys
import operator
import itertools
from enum import IntEnum

class AddrMode(IntEnum):
    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2


class Program:
    def __init__(self, data, input):
        self.data = list(data) + [0] * 1000
        self.input = input
        self.pc = 0
        self.ic = 0
        self.rb = 0
        self.finished = False

    def add_input(self, input):
        self.input += input

    def run(self):
        assert not self.finished
        data = self.data
        pc = self.pc
        ic = self.ic
        max_params = 3

        def get_param_addr(pos):
            mode = modes[pos - 1]
            if mode == AddrMode.IMMEDIATE:
                return pc + pos
            elif mode == AddrMode.RELATIVE:
                return data[pc + pos] + self.rb
            else:
                assert mode == AddrMode.POSITION
                return data[pc + pos]

        def write_param(pos, v):
            assert modes[pos - 1] != AddrMode.IMMEDIATE
            data[get_param_addr(pos)] = v

        def get_param(pos):
            return data[get_param_addr(pos)]

        output = []

        while True:
            inst = data[pc]
            opcode = inst % 100
            param_modes = inst // 100
            modes = []
            for i in range(max_params):
                modes.append(AddrMode(param_modes % 10))
                param_modes //= 10

            if opcode == 99:
                self.finished = True
                break
            elif opcode in [1, 2]:
                fn = [operator.add, operator.mul][opcode - 1]
                r = fn(get_param(1), get_param(2))
                write_param(3, r)
                pc += 4
            elif opcode == 3:
                if ic >= len(self.input):
                    break
                write_param(1, self.input[ic])
                ic += 1
                pc += 2
            elif opcode == 4:
                output.append(get_param(1))
                pc += 2
            elif opcode in [5, 6]:
                tv = get_param(1)
                tvz = tv == 0
                jiz = opcode == 6
                if tvz == jiz:
                    pc = get_param(2)
                else:
                    pc += 3
            elif opcode in [7, 8]:
                fn = [operator.lt, operator.eq][opcode - 7]
                write_param(3, int(fn(get_param(1), get_param(2))))
                pc += 4
            elif opcode == 9:
                self.rb += get_param(1)
                pc += 2

        self.pc = pc
        self.ic = ic

        return output


def main(lines):
    data = [int(i) for i in lines[0].split(',')]
    p = Program(data, [2])
    print(','.join(str(i) for i in p.run()))

if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

