#!/usr/bin/env python

import os
import sys


def main(lines):
    data = lines[0]
    final = '2' * (6 * 25)

    def resolve_pixel(old, new):
        if old == '2':
            return new
        return old

    for i in range(0, len(data), 25 * 6):
        layer = data[i:i + 25 * 6]

        final = ''.join(resolve_pixel(o, n) for o, n in zip(final, layer))

    def p_value(p):
        return 'X' if p == '1' else ' '

    for y in range(6):
        row = final[y * 25:y * 25 + 25]
        print(''.join(p_value(p) for p in row))



if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

