#!/usr/bin/env python

import os
import sys
import collections


def count_graph_edges(pairs):
    parent_s = set()
    child_s = set()
    edges = collections.defaultdict(list)
    parent_edge = {}

    def count_from_root(r, above=0):
        children = edges[r]
        return above + sum(count_from_root(c, above + 1) for c in children)


    for a, b in pairs:
        parent_s.add(a)
        child_s.add(b)
        edges[a].append(b)
        parent_edge[b] = a

    you_ancestors = {}
    p = parent_edge['YOU']
    c = 0
    while p:
        you_ancestors[p] = c
        c += 1
        p = parent_edge.get(p)

    p = parent_edge['SAN']
    c = 0
    while p:
        if p in you_ancestors:
            return c + you_ancestors[p]
        c += 1
        p = parent_edge.get(p)





def main(lines):
    pairs = [l.split(')') for l in lines]

    print(count_graph_edges(pairs))


if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

