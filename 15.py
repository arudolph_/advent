#!/usr/bin/env python

import os
import sys
import intcode


def main(lines):
    p = intcode.Program(lines[0], [])

    board = {}

    def print_board():
        min_x = min(p[0] for p in board)
        min_y = min(p[1] for p in board)
        max_x = max(p[0] for p in board)
        max_y = max(p[1] for p in board)

        for y in range(min_y, max_y + 1):
            print (''.join(
                board.get((x, y), ' ') for x in range(min_x, max_x + 1)))

    deltas = {1: (0, -1), 2: (0, 1), 3: (-1, 0), 4: (1, 0)}
    def opp(d):
        return d + 1 if d % 2 else d - 1

    def move(d):
        p.add_input([d])
        return p.run()[0]

    final_dist = o_p = None
    def explore_from(x, y, sofar):
        nonlocal o_p, final_dist
        board[(x, y)] = '.'

        for d, (dx, dy) in deltas.items():
            nx, ny = x + dx, y + dy
            if (nx, ny) in board:
                continue
            o = move(d)
            if o == 0:
                board[(nx, ny)] = '#'
            else:
                if o == 2:
                    o_p = (nx, ny)
                    final_dist = sofar + 1
                r = explore_from(nx, ny, sofar + 1)
                if r != False:
                    return r
                move(opp(d))
        return False
    explore_from(0, 0, 0)
    print(final_dist)

    s = 0
    board[o_p] = 'O'
    newos = [o_p]
    while '.' in board.values():
        next_newos = []
        for o in newos:
            x, y = o
            for dx, dy in deltas.values():
                nx, ny = x + dx, y + dy
                if board[(nx, ny)] == '.':
                    board[(nx, ny)] = 'O'
                    next_newos.append((nx, ny))
        newos = next_newos
        s += 1
    print(s)

if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

