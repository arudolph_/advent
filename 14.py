#!/usr/bin/env python

import os
import sys
import intcode


def main(lines):
    reactions = {}
    def handle_ingredient(i):
        p = i.split(' ')
        return (int(p[0]), p[1])
    def proc_line(l):
        ings, p = l.split(' => ')
        ings = [handle_ingredient(i) for i in ings.split(', ')]
        p = handle_ingredient(p)
        reactions[p[1]] = (p[0], ings)
    for l in lines:
        proc_line(l)


    t = int(1e12)

    h = 800000000
    l = 0
    while True:
        m = (h - l) // 2 + l
        extra = {}
        a = produce_fuel(reactions, extra, m)
        print(m, a)
        if a < t:
            l = m
        elif a > t:
            h = m
        else:
            break




def produce_fuel(reactions, extra, ct=1):
    need = [(ct, 'FUEL')]
    ore_count = 0
    while need:
        needed_amt, reactant = need.pop(0)

        if reactant in extra:
            needed_amt -= extra[reactant]
            if needed_amt < 0:
                extra[reactant] = -needed_amt
                needed_amt = 0
            else:
                del extra[reactant]

        produced_amt, ingredients = reactions[reactant]
        reaction_count = (needed_amt + (produced_amt - 1)) // produced_amt
        excess = produced_amt * reaction_count - needed_amt
        if excess:
            extra[reactant] = excess + extra.get(reactant, 0)

        for amt, i in ingredients:
            amt *= reaction_count
            if i == 'ORE':
                ore_count += amt
            else:
                need.append((amt, i))
    return ore_count




if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

