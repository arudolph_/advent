#!/usr/bin/env python

import os
import sys
import intcode
import collections
import itertools
import functools

def reachable(start, keys):
    q = collections.deque([start])

    kdists = {}
    dists = {start: 0}

    while q:
        pt = q.popleft()
        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            npt = (pt[0] + dx, pt[1] + dy)
            c = grid[npt[1]][npt[0]]
            if c == '#':
                continue
            if c.isupper() and c.lower() not in keys:
                continue
            if npt in dists:
                continue
            nd = dists[pt] + 1
            dists[npt] = nd
            if c.islower() and c not in keys:
                kdists[c] = (nd, npt)
            else:
                q.append(npt)
    return kdists


def reachable4(starts, keys):
    return {k: (d, pt, ix) for ix, start in enumerate(starts)
                           for k, (d, pt) in reachable(start, keys).items()}

@functools.lru_cache(maxsize=None)
def search(starts, keys=''):
    kdists = reachable4(starts, keys)
    if not kdists:
        return 0
    else:
        def nstarts(pt, ix):
            return tuple(pt if sx == ix else s for sx, s in enumerate(starts))

        def nkeys(k):
            return ''.join(sorted(keys + k))

        return min(dist + search(nstarts(pt, ix), nkeys(k)) for
                       k, (dist, pt, ix) in kdists.items())
    return ans


def main():
    starts = []
    pairs = itertools.product(range(len(lines[0])), range(len(lines)))
    starts = [(x, y) for x in range(len(lines[0]))
                     for y in range(len(lines)) if grid[y][x] == '@']
    print(search(tuple(starts)))


if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    grid = lines
    main()

