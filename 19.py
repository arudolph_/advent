#!/usr/bin/env python

import os
import sys
import intcode
import itertools
import math

def main(lines):
    def pulled(x, y):
        return bool(intcode.Program(lines[0], [x, y]).run()[0])

    pairs = itertools.product(range(50), range(50))
    print(sum(pulled(x, y) for x, y in pairs))

    y = 1000
    x = 0
    while True:
        if pulled(x, y):
            xrat = x / y
            break
        x += 1

    y = 100
    while True:
        x = math.floor(y * xrat)
        while not pulled(x, y):
            x += 1
        if pulled(x + 99, y - 99):
            print(10000 * x + y - 99)
            return
        y += 1


if __name__ == '__main__':
    bn = os.path.basename(sys.argv[0]).rstrip('.py')
    with open(f'{bn}.in') as f:
        lines = [l.strip() for l in f]
    main(lines)

